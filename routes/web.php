<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::view('/contact','contact');
Route::view('/about','about');
Route::view('/register','register');
Route::view('/services','services');
Route::view('/services1','services1');
Route::view('/services2','services2');
Route::view('/services3','services3');
Route::view('/services4','services4');
Route::view('/services5','services5');
Route::view('/services6','services6');
Route::view('/services7','services7');
Route::view('/services8','services8');
Route::view('/services9','services9');
Route::view('/services10','services10');
Route::view('/support','support');
Route::view('/terms','terms');
Route::view('/privecy','privecy');
Route::view('/aboutcontent','aboutcontent');
Route::view('/custom','custom');
Route::view('/stone','stone');
Route::view('/jewelleryAppersail','jewelleryAppersail');
Route::view('/renovative','renovative');
Route::view('/jewellerypolishing','jewellerypolishing');
Route::view('/jewellerycleaning','jewellerycleaning');
Route::view('subservice1','subservice1');
Route::view('subservice2','subservice2');
Route::view('payments','payment');
Route::view('communication','communication');
Route::view('refund','refund');
Route::view('ShippingPolicy','shipping');
// Route::view('signup','Signup');
// Route::view('signin','signin');
