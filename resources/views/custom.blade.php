@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="img/custom_jewellery.avif" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color:#c29600"> Why Custom Jewelry Repair Matters</h3>
        <h6>Preservation of Sentimental Value</h6>
        <p align="justify">Custom jewelry often represents milestones and cherished memories. Repairing these pieces ensures that the sentimental value remains intact for generations to come.</p>
        <h6>Economic Value</h6>
        <p align="justify">High-quality custom jewelry can be a significant financial investment. Repairing rather than replacing damaged pieces is often more cost-effective, preserving your initial investment.</p>
        <h6>Eco-Friendly Choice</h6>
        <p align="justify">Repairing jewelry is a sustainable option, reducing the demand for new materials and energy-intensive jewelry production. Custom jewelry repair is an essential service that helps preserve the sentimental and economic value of your cherished pieces.</p>
    </div>
</div>
@include('footer')