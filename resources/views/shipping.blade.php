@include('header')
<div class="container my-2">
    <h1 align="center"><span style="color: #c29600" >Shipping</span><span> Policy</span></h1>
<h3 style="color: gold" align="center">Shipping Policy of Rushabh Swarna Maligai LLP (Renox)</h3>
<p align="justify">Welcome to RUSHABH SWARNA MALIGAI LLP (Renox). We are dedicated to providing
    you with top-quality jewellery repair services with the utmost convenience. Please review our
    Shipping Policy below.</p>
{{-- <h3>1. Shipping Timelines:</h3> --}}
<h6>1. Shipping Timelines:</h6><p align="justify"> Standard Shipping: Orders for jewellery repair services are typically processed and
    dispatched within 1-2 business days after receiving your items at our facility.</p>
 <p align="justify"> Estimated Repair Time: Repair timelines may vary depending on the complexity of
    the repair and the extent of the work needed. We aim to complete repairs promptly
    and efficiently.
</p>
<h6>2. Shipping Rates:</h6><p align="justify">We offer complimentary shipping for sending your jewellery items to our facility for repair.</p>

<h6>3. Pickup for Repair:</h6> <p align="justify">Once you book a repair with us, our dedicated delivery personnel will be dispatched to
    collect your jewellery items for repair from your provided address. our own person will go to
    pick and deliver. we have all the necessary records and details of our staff people and they're
    here since 5+yrs. Delivery person will go with weighing machine and show the weight to
    customer and fill in the form with them then while delivering the goods he will show the
    weight again for customers.</p>
    <h6>4. Shipping Policy Updates:</h6><p align="justify">We reserve the right to update or modify our Shipping Policy at any time without prior
        notice. Any changes to our policy will be reflected on this page.</p>
        <br>
    <p align="justify">If you have any questions or concerns regarding our Shipping Policy or the repair process,
        please feel free to contact us at . Our dedicated team will be happy to assist you.</p>  
</div>

@include('footer')