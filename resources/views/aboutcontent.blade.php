@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .container {
        width: 100%;
        padding: 10px;
    }

    h1,
    h2,
    h3,
    h5 {
        margin-top: 0;
    }

    p,
    h3,
    h5 {
        margin-bottom: 15px;
    }

    @media (min-width: 768px) {
        /* For laptops and larger screens */
        .container {
            max-width: 800px;
            margin: auto;
        }
    }
</style>
<div class="container my-2">
    <h1 align="center"><span style="color:black;">About</span> <span style="color: black;">Us</span></h1>
    <h2 style="color: #c29600;">Renox Story</h2>
    <h3>About Us</h3>
    <p align="justify">Welcome to RUSHABH SWARNA MALIGAI LLP (Renox), where craftsmanship meets care, and your precious jewelry finds new life. At Renox, we understand that every piece of jewelry holds unique value, whether it's a family heirloom passed down through generations or a token of love and celebration. We are honored to be your trusted partner in jewelry repair, restoration, and enhancement.</p>
    <p align="justify">Why to buy new gold in today's sky high gold rates when you can repair and give your old jewellery a completely new and modern look.
    </p>
    <p align="justify">In renox we expect to reach out to your inner connection with your jewelry and understand your thoughts.
    </p>
    <p align="justify">We're in this industry for more then 46+yrs now and we hope to continue and give our customers the best services.
    </p>
<p align="justify" >We have our own in house repairing centre and experienced family like staff team which helps us with your jewelry safety and also quick services.</p>
<p align="justify">We have repaired and customised more then 2500+customer.
</p>
<p align="justify" >We're into repairing service since 40+yrs and now we hope to reach out to you with our website and application to make it easy for our customers in today's online generation and help you save your time and get your work done.
</p>
<p align="justify">We will examine your jewelry and will also provide you with certificate of authenticity and purity of your jewelry as we have our own gold testing machine at our workshop.</p>
 <p align="justify">We're here to build a trust in this digital world and to be grateful for that.</p>
<p align="justify">There will be complete transperacy and reasonable charges only we hope to build a bond that makes us give you our best service and help build happy customers all around.</p>
    
</div>
@include('footer')