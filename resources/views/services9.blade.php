@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/polish.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600">JEWELRY POLISHING</h3>
        <p align="justify">Indulge in the brilliance of our highly recommended jewelry polishing service to breathe new life into your beloved pieces. When your jewelry starts showing signs of wear, fading, or unsightly scratches, our polishing service is the key to restoring them to a pristine, showroom-worthy condition. The distinct advantage of jewelry polishing lies in its ability to effectively eliminate most scratches, especially beneficial for everyday items like rings..</p>
        <p align="justify">Unlike basic cleaning, our polishing service goes beyond, transforming worn or scratched items into clean and brilliantly bright pieces. The process ensures a remarkable difference in the overall appearance and longevity of your cherished jewelry.</p>
        <p align="justify">Should you have any inquiries about our jewelry polishing service or any other repair services, please don't hesitate to contact us. We are always eager to assist and discuss the diverse options available to you.</p>
        <p align="center"> Click here<i class="fa fa-arrow-down ms-2"></i></p>
        <a href="{{url('subservice1')}}" class="btn btn-primary py-3 px-6">Rose gold polishing</a>
        <a href="{{url('subservice2')}}" class="btn btn-primary py-3 px-6">Rodium white polishing</a>
    </div>
</div>
@include('footer')