<style>
    .custom-btn {
    background-color: #C29600;
}

</style>
<div class="container-fluid bg-dark text-light footer mt-5 pt-5 wow fadeIn" data-wow-delay="0.1s">
    <div class="container py-5">
        <div class="row g-5">
            <div class="col-lg-3 col-md-6">
                <h4 class="mb-4" style="color: #C29600">Address</h4>
                <p class=" text-light mb-2"><a href="https://maps.app.goo.gl/KArKDB29ok7YCBJC9"><i class="fa fa-map-marker-alt me-3" style="color:#C29600;"></i><span style="color:white;">Address - Natraj Market office no -204, s.v
                    road Malad West Mumbai 64</span></a></p>
                    <p class="mb-2"><a href="tel:+918369835402"><i class="fa fa-phone-alt me-3" style="color: #C29600;"></i><span style="color:white;">8369835402</span></a></p>

                    <p class="mb-2"><a href="mailto:renoxrsm@gmail.com"><i class="fa fa-envelope me-3" style="color:#C29600;"></i><span style="color:white;">renoxrsm@gmail.com</span></a></p>

                <div class="d-flex pt-2">
                    <a class="btn btn-outline-light btn-social" href="https://twitter.com/i/flow/login"><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-outline-light btn-social" href="https://www.facebook.com/login/"><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-outline-light btn-social" href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a>
                    <a class="btn btn-outline-light btn-social" href="whatsapp://send?phone=+918369835402"><i class="fab fa-whatsapp"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <h4 class="mb-4" style="color: #C29600">Services</h4>
                <a class="btn btn-link" href="{{url('services1')}}">
                    BRACELET & NECKLACE SHORTENING & LENGTHENING</a>
                <a class="btn btn-link" href="{{url('services2')}}" >
                    EARRINGS POST REPLACEMENTS</a>
                <a class="btn btn-link" href="{{url('services3')}}" >
                    EARRINGS SCREW BACK SERVICING</a>
                <a class="btn btn-link" href="{{url('services4')}}" >JEWELRY SOLDERING SERVICE</a>
                <a class="btn btn-link" href="{{url('services')}}" >CLICK HERE FOR MORE SERVICES</a>

            </div>
            <div class="col-lg-3 col-md-6">
                <!-- <h4 class="text-light mb-4">Quick Links</h4> -->
                <a class="btn btn-link" href="{{url('about')}}" >About Us</a>
                <a class="btn btn-link" href="{{url('contact')}}">Contact Us</a>
                <a class="btn btn-link" href="{{url('privecy')}}">privacy policies</a>
                <a class="btn btn-link" href="{{url('terms')}}">Terms & Condition</a>
                <a class="btn btn-link" href="{{url('support')}}">Support</a>
                <a class="btn btn-link" href="{{url('refund')}}">Refund & Cancellation</a>
                <a class="btn btn-link" href="{{url('ShippingPolicy')}}">Shipping Policy</a>
            </div>
            <div class="col-lg-3 col-md-6">
                <!-- <h4 class="text-light mb-4">Newsletter</h4> -->
                <!-- <p>Dolor amet sit justo amet elitr clita ipsum elitr est.</p> -->
                <div class="position-relative mx-auto" style="max-width: 400px;">
                    <!-- <input class="form-control border-0 w-100 py-3 ps-4 pe-5" type="text" placeholder="Your email"> -->
                    <button type="button" class="btn btn-light py-2 position-absolute top-0 end-0 mt-2 me-2 custom-btn"><a
                        href="{{url('register')}}" style="color: aliceblue;">Start Your Repair</a></button>

                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="copyright">
            <div class="row">
                <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                    &copy; <a class="border-bottom" href="#">2024 All Rights Reserved. Renox</a>
                </div>
                <div class="col-md-6 text-center text-md-end">
                    <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                    <!-- Designed By <a class="border-bottom" href="https://htmlcodex.com">HTML Codex</a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer End -->


<!-- Back to Top -->
<a href="#" class="btn btn-lg btn-lg-square rounded-0 back-to-top custom-btn">
    <i class="bi bi-arrow-up"></i>
    
</a>



<!-- JavaScript Libraries -->

<script src="lib/wow/wow.min.js"></script>
<script src="lib/easing/easing.min.js"></script>
<script src="lib/waypoints/waypoints.min.js"></script>
<script src="lib/counterup/counterup.min.js"></script>
<script src="lib/owlcarousel/owl.carousel.min.js"></script>
<script src="lib/isotope/isotope.pkgd.min.js"></script>
<script src="lib/lightbox/js/lightbox.min.js"></script>

<!-- Template Javascript -->
<script src="js/main.js"></script>
</body>

</html>