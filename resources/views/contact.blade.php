@include('header')
<div class="container-fluid bg-light overflow-hidden px-lg-0" style="margin: 1rem 0;">
    <div class="container contact px-lg-0">
        <div class="row g-0 mx-lg-0">
            <div class="col-lg-6 contact-text py-5 wow fadeIn" data-wow-delay="0.5s">
                <div class="p-lg-5 ps-lg-0">
                    <!-- <div class="section-title text-start"> -->
                        <h1 class="display-3 mb-3 animated slideInDown" style="color: #C29600" align="center">Contact Us</h1>
                    <!-- </div>
                    <p class="mb-4">The contact form is currently inactive. Get a functional and working contact form with Ajax & PHP in a few minutes. Just copy and paste the files, add a little code and you're done. <a href="https://htmlcodex.com/contact-form">Download Now</a>.</p> -->
                    <form>
                        <div class="row g-3">
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="text" class="form-control" id="name" placeholder="Your Name">
                                    <label for="name">Your Name</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="email" class="form-control" id="email" placeholder="Your Email">
                                    <label for="email">Your Email</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="number" name="quantity" maxlength="10" class="form-control" id="email" placeholder="Your Phone Number">
                                    <label for="email">Your Phone Number</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-floating">
                                    <input type="text" class="form-control" id="subject" placeholder="Subject">
                                    <label for="subject">Subject</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-floating">
                                    <textarea class="form-control" placeholder="Leave a message here" id="message" style="height: 100px"></textarea>
                                    <label for="message">Message</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-light" type="button" onclick="submitForm()" style="background-color:#C29600;">Send Message</button>

                            </div>
                            
                            
                        </div>
                    </form>
                    <script>
                        function submitForm() {
                          
                          var nameInput = document.getElementById('name');
                          var errorMessage = document.getElementById('errorMessage');
                          var successMessage = document.getElementById('successMessage');
                    
                          if (nameInput.value.trim() === '') {
                            errorMessage.style.display = 'block';
                            successMessage.style.display = 'none';
                          } else {
                            
                            errorMessage.style.display = 'none';
                            successMessage.style.display = 'block';
                          }
                        }
                      </script>
                </div>
            </div>
            <!-- <div class="col-lg-6 pe-lg-0" style="min-height: 400px;">
                <div class="position-relative h-100">
                    <iframe class="position-absolute w-100 h-100" style="object-fit: cover;"src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3768.1790007470604!2d72.84448145391781!3d19.187382577812!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7b7d2d7036d59%3A0x2c3e8a4ed7dd08b3!2sNATRAJ%20MARKET!5e0!3m2!1sen!2sin!4v1704733380116!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"
                    frameborder="0" allowfullscreen="" aria-hidden="false"
                    tabindex="0"></iframe>
                </div> -->
            </div>
        </div>
    </div>
</div>
@include('footer')