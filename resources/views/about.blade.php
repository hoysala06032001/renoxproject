@include('header')
 <!-- About Start -->
 <div class="container-fluid bg-light overflow-hidden my-3 px-lg-0">
    <div class="container about px-lg-0">
        <div class="row g-0 mx-lg-0">
            <div class="col-lg-6 ps-lg-0" style="min-height: 400px;">
                <div class="position-relative h-100">
                    <img class="img-fluid w-100 h-100" src="/img/homeabout1.png"  alt="" >
                </div>
            </div>
            <div class="col-lg-6 about-text py-5 wow fadeIn" data-wow-delay="0.5s">
                <div class="p-lg-5 pe-lg-0">
                    <div class="">
                        <h1 class="display-3 mb-3 animated slideInDown" style="color: #C29600">About Us</h1>
                    </div>
                    <p class="mb-4 pb-2" align="justify">Welcome to RUSHABH SWARNA MALIGAI LLP (Renox), where craftsmanship meets care, and your precious jewelry finds new life. At Renox, we understand that every piece of jewelry holds unique value, whether it's a family heirloom passed down through generations or a token of love and celebration. We are honored to be your trusted partner in jewelry repair, restoration, and enhancement
                    </p>
                    <a href="{{url('aboutcontent')}}" class="btn btn-light py-3 px-6" style="background-color:#C29600;" >Explore More...</a>.
                    <div class="row g-4 mb-4 pb-2 my-2">
                        <div class="col-sm-6 wow fadeIn" data-wow-delay="0.1s">
                            <div class="d-flex align-items-center">
                                <div class="d-flex flex-shrink-0 align-items-center justify-content-center bg-white" style="width: 60px; height: 60px;">
                                    <i class="fa fa-users fa-2x text-primary"></i>
                                </div>
                                <div class="ms-3">
                                    <h2 class="text-primary mb-1" data-toggle="counter-up">1234</h2>
                                    <p class="fw-medium mb-0">Happy Clients</p>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About End -->

@include('footer')