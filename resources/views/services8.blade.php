@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/engraving.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600">JEWELRY ENGRAVING</h3>
        <p align="justify">Engraving provides a unique opportunity to infuse meaning into your cherished items, making them truly one-of-a-kind. Whether it's a special date, a heartfelt message, or a symbol with personal significance, our skilled craftsmen can bring your vision to life.</p>
        <p align="justify">Why settle for ordinary when you can transform your watch or jewelry into a personalized masterpiece? Elevate the emotional connection to your pieces by exploring the diverse engraving options available. Let your imagination run wild as you consider the endless possibilities to make your possessions truly special and uniquely yours.</p>
    </div>
</div>
@include('footer')