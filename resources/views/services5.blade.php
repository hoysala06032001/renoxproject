@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/clasp.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600">JEWELRY CLASP REPLACEMENT</h3>
        <p align="justify">
            If your jewelry clasp is damaged, we provide a comprehensive clasp replacement service. Our skilled artisans attach or solder a new clasp onto your necklace or bracelet, using the same metal as the original item, such as 9ct gold or silver.</p>
        <p align="justify">It's crucial to prioritize a proper clasp repair to avoid potential mishaps. A poorly executed repair may lead to your bracelet or necklace unexpectedly falling off, risking damage or, in the worst-case scenario, loss.</p>
        <p align="justify">Often, individuals may mistakenly believe their necklace or bracelet is beyond repair. Opting for a clasp replacement ensures you can continue to enjoy your favorite piece of jewelry without compromise. We recommend regular checks of the clasps on your bracelets and necklaces, especially those worn daily. They undergo natural wear and tear, but unfortunately, this becomes apparent only when it's too late, resulting in the item falling off or, in more severe cases, getting lost. Make it a habit to inspect yours regularly to avoid such unforeseen situations.</p>
    </div>
</div>
@include('footer')