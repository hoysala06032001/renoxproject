@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/claws.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600">JEWELRY CLAWS RETAPPED</h3>
        <p align="justify">Regular checks on your claws are recommended, not just through visual inspection but also by tactile examination. Run your fingers over them to detect any roughness or sharpness that may have developed over time.

        </p>

        <p align="justify">It's essential to extend this scrutiny to your rings, earrings, and any pendants with a similar setting, especially those worn daily. Earrings, in particular, can be easily overlooked. Neglecting timely claw re-tipping may lead to a loose stone, potential damage to your ring, earring, or pendant, and, in the worst-case scenario, a lost gem.</p>
    </div>
</div>
@include('footer')