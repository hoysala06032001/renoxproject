@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/piercing.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600">Piercing Service</h3>
        <p align="justify">
            At RENOX Jewelry, our skilled ear piercers have undergone rigorous training by industry experts. They are dedicated to delivering flawless piercings consistently, ensuring your satisfaction every time.</p>
        <p align="justify">Our professionals prioritize hygiene and take meticulous precautions before and during the ear piercing procedure. We maintain strict standards to uphold cleanliness and safety for all our customers.</p>
        <p align="justify">To guarantee a swift and painless ear piercing experience, our experts conduct the procedure with utmost care, adhering to stringent safety measures. You can trust us to provide a comfortable and worry-free experience, ensuring your ear piercing journey with RENOX Jewelry is both pleasant and memorable.</p>
        <p align="justify">Moreover, RENOX Jewelry goes the extra mile by offering door-to-door piercing services. Enjoy the convenience of getting your ears pierced in the comfort of your own home. We bring our expertise directly to you, ensuring a convenient and personalized experience tailored to your needs.</p>
        
    </div>
</div>
@include('footer')