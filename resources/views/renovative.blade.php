@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/renovative.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600">Jewelry Renovation at Renox</h3>
        <h6>Craftsmanship</h6>
        <p align="justify">Our skilled artisans combine traditional craftsmanship with modern techniques to ensure your jewelry's quality and aesthetics are elevated to the highest standards.</p>
        <h6>Customization</h6>
        <p align="justify">We understand that every piece is unique. Our Rennovation services are tailored to your preferences, whether you're seeking a modern update or a vintage restoration.</p>
        <h6>Sentimental Preservation</h6>
        <p align="justify">Your jewelry holds memories and sentiment. We take great care in preserving the emotional connection while enhancing its beauty.</p>
        <p align="justify" >Renox Jewelry's Rennovation services offer a unique opportunity to rekindle your love for your jewelry. Whether you wish to redesign, upgrade, or restore your pieces, our artisans are dedicated to creating jewelry that reflects your style and preserves its sentimental value. Elevate your jewelry with Renox, where craftsmanship meets creativity, and cherished memories are beautifully reborn.</p>
    </div>
</div>

@include('footer')