@include('header')
<div class="container my-2">
    <h1 align="center"><span style="color: #c29600" >Refund</span> & <span>Cancellation </span></h1>
<h3 style="color: gold" align="center">Refund and Cancellation Policy for Jewellery Repair Services and Item Delivery of Renox</h3>
<p align="justify">At Renox, we are committed to providing exceptional jewellery repair services and ensuring a seamless experience for our valued customers. Our refund and cancellation policy is designed to clarify the terms and conditions associated with our jewellery repair services and the delivery of repaired items to your address.</p>
<h3>Refund Policy for Jewellery Repair Services:</h3>
<h6>1. Quality Assurance:</h6><p align="justify"> Renox takes pride in delivering high-quality jewellery repair services. If you are unsatisfied with the repair work due to a fault on our end, we will rectify the issue at no additional cost.</p>
<h6>2. Refund Eligibility:</h6> <p align="justify">Refunds are only applicable if the jewellery repair service falls below our standard quality, and the issue is reported within 7 days of receiving the repaired item.
</p>
<h6>3. Communication:</h6><p align="justify">Please communicate any concerns or issues regarding the repair service promptly. Our customer support team is ready to assist you in resolving any problems.</p>

<h6>4. Non-refundable Items:</h6> <p align="justify">Customized or personalized jewellery repair services are non-refundable once the work has commenced, as they are tailored to your specific requirements.</p>
<h3 style="color: gold" align="center">Cancellation Policy for Jewellery Repair Services</h3>

    <h6>1. Cancellation Window:</h6><p align="justify"> If you wish to cancel your jewellery repair service, please do so within 24 hours of placing the order. Cancellations beyond this timeframe may incur a cancellation fee.</p>
   <h6>2. Cancellation Fee:</h6>  <p align="justify">Cancellations made after 24 hours of placing the order may be subject to a cancellation fee to cover administrative and processing costs.</p>
    <h6>3. Refund Processing Time:</h6> <p align="justify">Refunds for cancelled services will be processed within 7-10 business days, and the amount refunded will be the original service cost minus any applicable cancellation fees.</p>
    <h3>Item Delivery to User Address:</h3>
<h6>1. Delivery Timeframe: </h6><p align="justify">Once the jewellery repair is completed, we aim to dispatch the repaired item to your provided address within the stipulated timeframe mentioned during the order placement.</p>
<h6>2. Shipping Fees:</h6><p align="justify"> Standard shipping fees apply for the delivery of repaired items to your address. Expedited shipping options may be available at an additional cost.</p>
<h6>3. Tracking Information:</h6><p align="justify"> A tracking number will be provided once the item is shipped, allowing you to monitor the delivery status.</p>

<h6>4. Delivery Issues:</h6> <p align="justify">In the rare event of a delivery issue, please contact our customer support team immediately for assistance in tracking or resolving the matter.</p>
</div>

@include('footer')