@include('header')

<style>
    .overlay-text {
    transform: translate(-50%, -50%);
    color: white;
    z-index: 1;
    
    .custom-btn {
    background-color: red;
}

}
.content {
        text-align: center;
        margin-top: 20px;
    }

    .blink {
        animation: blink 1.5s linear infinite;
        color: black;
        text-align: center;
        margin-bottom: 10px;
        font-size: 20px;
        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
    }

    @keyframes blink {
        0% {
            opacity: 1;
        }
        50% {
            opacity: 0;
        }
        100% {
            opacity: 1;
        }
    }
/* body {
            min-height: 100vh;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: flex-end;
            margin: 0;
        } */

        .col-12::before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(52, 52, 52, 0.75); /* Adjust color and opacity as needed */
    z-index: 1; /* Ensure the overlay is above the video */
}


</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
<marquee direction="left" behavior="scroll" scrollamount="infinity">
    <i class="fas fa-gem" style="color: gold;"></i>
    Start Your Repair Available In Mumbai And Chennai
        <i class="fas fa-gem" style="color: gold;"></i>
</marquee>
<div class="container-fluid">
    <div class="row">
        <div class="col-12 p-0 position-relative">
            <!-- Add your video section here -->
            <div style="position: relative; overflow: hidden; padding-top: 56.25%;">
                <!-- The 56.25% padding-top is for a 16:9 aspect ratio, adjust as needed -->
                <video src="/img/jewellery_video2.mp4" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" muted autoplay loop>
                </video>
                <div class="overlay-text position-absolute top-50 start-50 translate-middle text-center text-white">
                    <!-- Add your introductory words within the video content -->
                    {{-- <h6 class="text-white text-uppercase mb-3 animated slideInDown" style="color: #C29600">Welcome To Renox</h6> --}}
                    <p class="fs-9 fw-medium text-white mb-4 pb-2 animated slideInDown" style="font-family:fantasy">Welcome to RENOX (Rushabh Swarna Maligai LLP), where craftsmanship meets care and your precious jewelry finds new life.</p>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Carousel Start -->
{{-- <div class="container-fluid p-0 pb-5">
    <div class="owl-carousel header-carousel position-relative">
        <div class="owl-carousel-item position-relative">
            <video class="d-block w-100" src="img/jewellery_video3.mp4" autoplay muted></video>
            <div class="position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center" style="background: rgba(53, 53, 53, .7);">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-8 text-center">
                            <h5 class="text-white text-uppercase mb-3 animated slideInDown">Renox</h5>
                            <h1 class="display-3 text-white animated slideInDown mb-4">Welcome To Renox</h1>
                            <p class="fs-5 fw-medium text-white mb-4 pb-2">Welcome to Renox, where craftsmanship meets care, and your precious jewelry finds new life. At Renox, we understand that every piece of jewelry holds unique value, whether it's a family heirloom passed down through generations or a token of love and celebration. We are honored to be your trusted partner in jewelry repair, restoration, and enhancement</p>
                            <a href="{{url('about')}}" class="btn btn-primary py-md-3 px-md-5 me-3 animated slideInLeft">Read More</a>
                            <!-- <a href="" class="btn btn-light py-md-3 px-md-5 animated slideInRight">Free Quote</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="owl-carousel-item position-relative">
            <img class="img-fluid" src="img/carousel-2.jpg" alt="">
            <div class="position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center" style="background: rgba(53, 53, 53, .7);">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-8 text-center">
                            <h5 class="text-white text-uppercase mb-3 animated slideInDown"></h5>
                            <h1 class="display-3 text-white animated slideInDown mb-4">Best Carpenter & Craftsman Services</h1>
                            <p class="fs-5 fw-medium text-white mb-4 pb-2">Vero elitr justo clita lorem. Ipsum dolor at sed stet sit diam no. Kasd rebum ipsum et diam justo clita et kasd rebum sea elitr.</p>
                            <a href="" class="btn btn-primary py-md-3 px-md-5 me-3 animated slideInLeft">Read More</a>
                            <a href="" class="btn btn-light py-md-3 px-md-5 animated slideInRight">Free Quote</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>  -->
        <div class="owl-carousel-item position-relative">
            <video class="d-block w-100" src="img/jewellery_video2.mp4" autoplay muted></video>
            <div class="position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center" style="background: rgba(53, 53, 53, .7);">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-8 text-center">
                            <h5 class="text-white text-uppercase mb-3 animated slideInDown">Welcome To Renox</h5>
                            <h1 class="display-3 text-white animated slideInDown mb-4"></h1>
                            <p class="fs-5 fw-medium text-white mb-4 pb-2">
                                enox was founded on the principles of craftsmanship, attention to detail, and a deep appreciation for the artistry of fine jewelry. We take pride in our team of highly skilled artisans, each with a wealth of experience in working with various jewelry styles and materials. Our artisans possess a genuine passion for preserving the beauty and significance of your cherished pieces.
                            </p>
                            <!-- <a href="" class="btn btn-primary py-md-3 px-md-5 me-3 animated slideInLeft">Read More</a>
                            <a href="" class="btn btn-light py-md-3 px-md-5 animated slideInRight">Free Quote</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<br>

<!-- Service Start -->
<div class="container-xxl py-3">
    <div class="container">
        <!-- <div class="section-title text-center"> -->
        <h1 align="center" style="color: #C29600"> Our Service</h1>
    </div>
    <a href="{{url('services')}}"><div class="content">
        <p class="blink mb-0">
            <h6 class="blink">Get Your 1st Repair With Renox For Free <i class="fas fa-truck flip-horizontal" style="color: gold;"></i></h6>
        </p>
    </div>
</a>
    <div class="row g-4">
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="/img/serrr1 3.jpg" alt="" >
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">
                        BRACELET & NECKLACE
                        SHORTENING & LENGTHENING</h4>
                    <p>At Renox, our professional bracelet and necklace shortening and lengthening services.</p>
                    <a class="fw-medium"  href="{{url('services1')}}" style="color: #C29600">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="img/post_earring.jpg" alt="">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">EARRINGS POST REPLACEMENT SERVICE</h4>
                    <p> Earrings are more than just accessories; they are statements of style.</p>
                    <a class="fw-medium" href="{{url('services2')}}" style="color: #C29600">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.5s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="img/earring_screw.jpg" alt="">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">EARRINGS SCREW BACK SERVICING</h4>
                    <p>Earrings are not just adornments; they are a reflection of your style and taste.</p>
                    <a class="fw-medium" href="{{url('services3')}}" style="color: #C29600">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="img/soldering.jpg" alt="">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">JEWELRY SOLDERING SERVICE</h4>
                    <p>Jewelry is more than just decorative pieces they are timeless expressions of your style.</p>
                    <a class="fw-medium" href="{{url('services4')}}" style="color: #C29600">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="service-item">
                <div class="overflow-hidden" style="max-width: 100%; height: auto;">
                    <img class="img-fluid" src="img/clasp.jpg" alt="" style="height: auto;">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">JEWELRY CLASP REPLACEMENT</h4>
                    <p>Jewelry is a reflection of your personal style and holds significant sentimental value.</p>
                    <a class="fw-medium" href="{{url('services5')}}" style="color: #C29600">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.5s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="img/claws.jpg" alt="">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">Click here for more services</h4>
                    <p>We understand the importance of maintaining your jewelry's radiant shine.</p>
                    <a class="fw-medium" href="{{url('services')}}" style="color: #C29600">click here for  More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        {{-- <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="img/jewellery_cleaning.jpg" alt="">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">JEWELRY CLEANING SERVICE</h4>
                    <p>We understand the importance of maintaining your jewelry's radiant shine.</p>
                    <a class="fw-medium" href="{{url('services7')}}">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="img/engraving.jpg" alt="">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">JEWELRY ENGRAVING SERVICE</h4>
                    <p>Personalized jewelry, engraved with names, dates, or special messages.</p>
                    <a class="fw-medium" href="{{url('services8')}}">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="img/jewellery_polishing.jpg" alt="">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">JEWELRY POLISHING SERVICE</h4>
                    <p>Jewelry, with its timeless elegance, captures moments and emotions that define our lives.</p>
                    <a class="fw-medium" href="{{url('services9')}}">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div> --}}
            {{-- </div> --}}
        {{-- </div> --}}
    </div>
</div>
</div>
<!-- Service End -->
<!-- About Start -->
<div class="container-fluid bg-light overflow-hidden my-3 px-lg-0">
    <div class="container about px-lg-0">
        <div class="row g-0 mx-lg-0">
            <div class="col-lg-6 ps-lg-0" style="min-height: 400px;">
                <div class="position-relative h-100">
                    <img class="img-fluid w-100 h-100" src="/img/homeabout1.png"  alt="" >
                </div>
            </div>
            <div class="col-lg-6 about-text py-5 wow fadeIn" data-wow-delay="0.5s">
                <div class="p-lg-5 pe-lg-0">
                    <div class="">
                        <h1 align="center" style="color: #C29600">About Us</h1>
                    </div>
                    <p class="mb-4 pb-2" align="justify">Welcome to RUSHABH SWARNA MALIGAI LLP (Renox), where craftsmanship meets care, and your precious jewelry finds new life. At Renox, we understand that every piece of jewelry holds unique value, whether it's a family heirloom passed down through generations or a token of love and celebration. We are honored to be your trusted partner in jewelry repair, restoration, and enhancement
                    </p>
                    <a href="{{url('aboutcontent')}}" class="btn btn-light py-3 px-6 custom-btn">Explore More...</a>

                    <div class="row g-4 mb-4 pb-2 my-2">
                        <div class="col-sm-6 wow fadeIn" data-wow-delay="0.1s">
                            <div class="d-flex align-items-center">
                                <div class="d-flex flex-shrink-0 align-items-center justify-content-center bg-white" style="width: 60px; height: 60px;">
                                    <i class="fa fa-users fa-2x text-primary" ></i>
                                </div>
                                <div class="ms-3" style="color: #C29600">
                                    <h2 class="text-primary mb-1" data-toggle="counter-up">1234</h2>
                                    <p class="fw-medium mb-0">Happy Clients</p>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About End -->
<!-- Projects Start -->
<div class="container-xxl py-5">
    <div class="container">
        <!-- <div class="section-title text-center"> -->
            <h1 align="center" style="color: #C29600">Why Repair With Renox</h1>
        </div>
        <!-- <div class="row mt-n2 wow fadeInUp" data-wow-delay="0.3s">
            <div class="col-12 text-center">
                <ul class="list-inline mb-5" id="portfolio-flters">
                    <li class="mx-2 active" data-filter="*">All</li>
                    <li class="mx-2" data-filter=".first">General Carpentry</li>
                    <li class="mx-2" data-filter=".second">Custom Carpentry</li>
                </ul>
            </div>
        </div> -->
        <div class="row g-4 portfolio-container">
            <div class="col-lg-4 col-md-6 portfolio-item first wow fadeInUp" data-wow-delay="0.1s">
                <div class="rounded overflow-hidden">
                    <div class="position-relative overflow-hidden">
                        <img class="img-fluid w-100" src="img/custom_jewellery.avif" alt="">
                        <div class="portfolio-overlay">
                            <a class="btn btn-square btn-outline-light mx-1" href="img/custom_jewellery.avif" data-lightbox="portfolio"><i class="fa fa-eye"></i></a>
                            <a class="btn btn-square btn-outline-light mx-1" href="{{url('custom')}}"><i class="fa fa-link"></i></a>
                        </div>
                    </div>
                    <div class="border border-5 border-light border-top-0 p-4">
                        <p class=" fw-medium mb-2" style="color: #C29600">Custom Jewelry Design</p>
                        <p class="lh-base mb-0" align="justfy">Custom jewelry holds a special place in our hearts <a href="{{url('custom')}}" style="color: #C29600">read more...</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item second wow fadeInUp" data-wow-delay="0.3s">
                <div class="rounded overflow-hidden">
                    <div class="position-relative overflow-hidden">
                        <img class="img-fluid w-100" src="img/stone_replacement.jpg" alt="">
                        <div class="portfolio-overlay">
                            <a class="btn btn-square btn-outline-light mx-1" href="img/stone_replacement.jpg" data-lightbox="portfolio"><i class="fa fa-eye"></i></a>
                            <a class="btn btn-square btn-outline-light mx-1" href="{{url('stone')}}"><i class="fa fa-link"></i></a>
                        </div>
                    </div>
                    <div class="border border-5 border-light border-top-0 p-4">
                        <p class=" fw-medium mb-2" style="color: #C29600">Stone Replacement</p>
                        <p class="lh-base mb-0">Renox Jewelry understands that every piece of jewelry tells a story <a href="{{url('stone')}}" style="color: #C29600">read more..</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item first wow fadeInUp" data-wow-delay="0.5s">
                <div class="rounded overflow-hidden">
                    <div class="position-relative overflow-hidden">
                        <img class="img-fluid w-100" src="img/jewellery_apprisal.jpg" alt="">
                        <div class="portfolio-overlay">
                            <a class="btn btn-square btn-outline-light mx-1" href="img/jewellery_apprisal.jpg" data-lightbox="portfolio"><i class="fa fa-eye"></i></a>
                            <a class="btn btn-square btn-outline-light mx-1" href="{{url('jewelleryAppersail')}}"><i class="fa fa-link"></i></a>
                        </div>
                    </div>
                    <div class="border border-5 border-light border-top-0 p-4">
                        <p class="fw-medium mb-2" style="color: #C29600">Jewelry Appraisal</p>
                        <p class="lh-base mb-0" >At Renox Jewelry, we understand that every piece of jewelry holds <a href="{{url('jewelleryAppersail')}}" style="color: #C29600">read more..</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item second wow fadeInUp" data-wow-delay="0.1s">
                <div class="rounded overflow-hidden">
                    <div class="position-relative overflow-hidden">
                        <img class="img-fluid w-100" src="img/renovative.jpg" alt="">
                        <div class="portfolio-overlay">
                            <a class="btn btn-square btn-outline-light mx-1" href="img/renovative.jpg" data-lightbox="portfolio"><i class="fa fa-eye"></i></a>
                            <a class="btn btn-square btn-outline-light mx-1" href="{{url('renovative')}}"><i class="fa fa-link"></i></a>
                        </div>
                    </div>
                    <div class="border border-5 border-light border-top-0 p-4">
                        <p class=" fw-medium mb-2" style="color: #C29600">Rennovate Jewelry</p>
                        <p class="lh-base mb-0">Our Rennovation services are designed to breathe new life into your jewelry <a href="{{url('renovative')}}" style="color: #C29600">read more..</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item first wow fadeInUp" data-wow-delay="0.3s">
                <div class="rounded overflow-hidden">
                    <div class="position-relative overflow-hidden">
                        <img class="img-fluid w-100" src="img/jewellery_polishing.jpg" alt="">
                        <div class="portfolio-overlay">
                            <a class="btn btn-square btn-outline-light mx-1" href="img/jewellery_polishing.jpg" data-lightbox="portfolio"><i class="fa fa-eye"></i></a>
                            <a class="btn btn-square btn-outline-light mx-1" href="{{url('jewellerypolishing')}}"><i class="fa fa-link"></i></a>
                        </div>
                    </div>
                    <div class="border border-5 border-light border-top-0 p-4">
                        <p class="fw-medium mb-2" style="color: #C29600">JewelryPolishing</p>
                        <p class="lh-base mb-0">We at Renox, believe that every piece of jewelry deserves to shine <a href="{{url('jewellerypolishing')}}" style="color: #C29600">read more..</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item second wow fadeInUp" data-wow-delay="0.5s">
                <div class="rounded overflow-hidden">
                    <div class="position-relative overflow-hidden">
                        <img class="img-fluid w-100" src="img/jewellery_cleaning.jpg" alt="">
                        <div class="portfolio-overlay">
                            <a class="btn btn-square btn-outline-light mx-1" href="img/jewellery_cleaning.jpg" data-lightbox="portfolio"><i class="fa fa-eye"></i></a>
                            <a class="btn btn-square btn-outline-light mx-1" href="{{url('jewellerycleaning')}}"><i class="fa fa-link"></i></a>
                        </div>
                    </div>
                    <div class="border border-5 border-light border-top-0 p-4">
                        <p class="fw-medium mb-2"style="color: #C29600">JewelryCleaning</p>
                        <p class="lh-base mb-0">At Renox, we recognize that every piece of jewelry tells a unique story <a href="{{url('jewellerycleaning')}}" style="color: #C29600">read more..</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Projects End -->

<!-- Testimonial Start -->
<div class="container-xxl py-5 wow fadeInUp" data-wow-delay="0.1s">
    <div class="container">
        <div class="text-center">
            <h1 class="display-5 mb-5" style="color: #C29600">Testimonial</h1>
        </div>
        <div class="owl-carousel testimonial-carousel">
            <div class="testimonial-item text-center">
                {{-- <img class="img-fluid bg-light p-2 mx-auto mb-3" src="img/testimonial-1.jpg" style="width: 90px; height: 90px;"> --}}
                <div class="testimonial-text text-center p-4">
                    <p>What truly impressed me was their dedication to preserving the integrity and beauty of each piece. The craftsmanship displayed in the repairs was nothing short of remarkable. Whether it was fixing a delicate necklace chain or restoring the luster of a family heirloom ring, every detail was handled with precision and care.</p>
                    <h5 class="mb-1">Ram Pandith</h5>
                    {{-- <span class="fst-italic">Profession</span> --}}
                </div>
            </div>
            <div class="testimonial-item text-center">
                {{-- <img class="img-fluid bg-light p-2 mx-auto mb-3" src="img/testimonial-2.jpg" style="width: 90px; height: 90px;"> --}}
                <div class="testimonial-text text-center p-4">
                    <p>The repair process itself was a testament to Renox's commitment to excellence. Every detail, from the intricate soldering work to the meticulous polishing, was executed with precision and finesse. When I received my bracelet back, it gleamed with a renewed brilliance, surpassing even its original allure.</p>
                    <h5 class="mb-1">Arjun</h5>
                    {{-- <span class="fst-italic">Profession</span> --}}
                </div>
            </div>
            <div class="testimonial-item text-center">
                {{-- <img class="img-fluid bg-light p-2 mx-auto mb-3" src="img/testimonial-3.jpg" style="width: 90px; height: 90px;"> --}}
                <div class="testimonial-text text-center p-4">
                    <p>I wholeheartedly recommend Renox Jewellery Repair to anyone seeking expert craftsmanship and outstanding service. The experience with Renox has been nothing short of exceptional, and I am grateful to Renox and his team for their professionalism, expertise, and genuine care.</p>
                    <h5 class="mb-1">Roopa</h5>
                    {{-- <span class="fst-italic">Profession</span> --}}
                </div>
            </div>
        </div>
    </div>
</div> 
@include('footer')
