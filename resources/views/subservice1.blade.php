@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/Rose.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600">Rose gold Polishing</h3>
        <p align="justify">At Renox, we understand the sentimental value and beauty that rose gold jewelry holds. Our dedicated team of skilled artisans is committed to restoring and enhancing the allure of your treasured rose gold pieces. Whether it's a family heirloom, engagement ring, bracelet, or any other rose gold accessory, we provide specialized repair services to bring your jewelry back to its original splendor.</p>
        <h4 align="center">Why Choose Renox for Rose Gold Repairs?</h4>
        <p align="justify"><h5>Expert Craftsmanship: </h5> <p align="justify">Our team comprises skilled artisans with a passion for preserving the beauty of rose gold jewelry.</p></p>
        <p align="justify"><h5>Quality Materials:</h5> <p align="justify">We use high-quality materials in our repairs to ensure longevity and durability.</p></p>
        <p align="justify"><h5>Timely Service:</h5> <p align="justify">We understand the value of your time. Our goal is to provide efficient and timely repair services without compromising on quality.</p></p>
        <p align="justify"><h5>Transparent Communication:</h5> <p align="justify">We keep you informed at every step of the repair process, ensuring transparency and peace of mind.</p></p>
    </div>
</div>
@include('footer')