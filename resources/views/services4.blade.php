@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/soldering.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600">JEWELRY SOLDERING SERVICE</h3>
        <p align="justify">
            We specialize in jewelry soldering for various jewelry items, including repairing broken rings, necklaces, or bracelets. Our skilled craftsmen employ solder that matches the carat of your item, whether it's 9ct, 14ct, 18ct, white gold, or yellow gold. Soldering is a highly sought-after service, given its effectiveness in properly restoring and strengthening jewelry pieces.</p>
        <p align="justify">As our jewelers meticulously solder the broken section, they go beyond mere repair by conducting a comprehensive 'health check' or an 'MOT' on your jewelry. This involves a thorough inspection of the entire item. If any additional issues are identified, we prioritize transparency by promptly informing you. Our detailed communication includes specifics such as the cost and time required for any suggested repairs.</p>
        <h6>Quality Materials:</h6>
        <p align="justify">Choose our jewelry soldering services not just for impeccable repairs but also for the added benefit of a thorough assessment, ensuring your cherished jewelry receives the care and attention it deserves.</p>
    </div>
</div>
@include('footer')