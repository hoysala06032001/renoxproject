@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; 
        margin: auto;
    }

    .ab{
        width: 100%; 
        height: auto;
        max-width: 500px; 
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/jewellery_cleaning.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600">Why Choose Renox's Professional Jewelry Cleaning Services?</h3>
        <h6>Expertise You Can Trust</h6>
        <p align="justify">
            Our highly-trained jewelers possess a deep understanding of various jewelry types, materials, and
            gemstones. With years of experience, they know the ins and outs of jewelry cleaning, ensuring that each
            piece is handled with the utmost care and precision.</p>
        <h6 align="slideInRight">State-of-the-Art Equipment</h6>
        <p align="justify">We invest in cutting-edge ultrasonic and steam cleaning machines to provide your jewelry
            with a comprehensive cleaning experience. These technologies allow us to remove dirt, grime, and tarnish
            from every nook and cranny, even in intricate settings.</p>
        <h6>Customized Approach</h6>
        <p align="justify">
            No two pieces of jewelry are the same, and neither are their cleaning needs. Our experts tailor their
            approach to your specific jewelry, taking into account its age, materials, gemstones, and overall
            condition. This personalized care guarantees the best possible results.</p>
        <h6>Inspection and Maintenance</h6>
        <p align="justfy">Beyond cleaning, our professionals conduct a meticulous inspection of your jewelry. This
            enables us to identify any loose stones, worn prongs, or potential issues. We'll provide recommendations
            for maintenance or repairs, ensuring your jewelry remains in top-notch condition.</p>
        <h6>Safe for All Types of Jewelry</h6>
        <p align="justfy">Our cleaning methods are gentle yet effective, making them suitable for a wide range of
            jewelry, including delicate antique pieces, valuable heirlooms, and contemporary designs.</p>
    </div>
</div>
@include('footer')