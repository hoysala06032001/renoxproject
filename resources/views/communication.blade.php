@include('header')
<style>
    .content {
        text-align: center;
        margin-top: 20px;
    }

    .blink {
        animation: blink 1.5s linear infinite;
        color: black;
        text-align: center;
        margin-bottom: 10px;
        font-size: 20px;
        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
    }

    @keyframes blink {
        0% {
            opacity: 1;
        }
        50% {
            opacity: 0;
        }
        100% {
            opacity: 1;
        }
    }

    /* .my {
        display: flex;
        flex-direction: column;
        align-items: center;
        text-align: center;
        max-width: 600px;
        margin: 0 auto;
    }

    .my .tel-div,
    .my .whatsapp-div {
        margin-bottom: 20px;
    }

    .my a {
        display: block;
        text-align: center;
    }

    .my img {
        max-width: 100%;
        height: auto;
    }

    @media (min-width: 768px) {
        .my {
            flex-direction: row;
            justify-content: space-between;
        }
        .my .tel-div,
        .my .whatsapp-div {
            margin-bottom: 0;
        }
    } */
    .me{
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
            /* background-color: #f4f4f4; */
            display: flex;
            justify-content: center;
            align-items: center;
            height: 80vh;
        } 
        .phone-container {
            margin-top:10px;
            /* background-color: #000; */
            border-radius: 60px;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.5);
            overflow: hidden;
            max-width: 250px;
            width: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-between;
            height: 499px;
            background-image: url('/img/iphone-removebg-preview.png') ; 
            background-size: 250px 500px;
            /* margin-bottom: 100px; */
        }


        /* h1 {
            color: #fff;
            text-align: center;
            padding: 1em;
        } */

        main {
            padding: 35px;
            display: flex;
            justify-content: space-around; /* Adjust as needed */
            width: 100%;
        }

        .contact-option1 {
            background-color: #fff;
            background-image: url('/img/phone.jpg');
            background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            padding: 12px;
            text-align: center;
            display: flex;
            flex-direction: column;
            align-items: center;
            height: 50px; /* Adjust the height for each option */
            width: 50px;
             /* Adjust the width for each option */
        }
        .contact-option2 {
            background-color: #fff;
            background-image: url('/img/whats.png');
            background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            padding: 12px;
            text-align: center;
            display: flex;
            flex-direction: column;
            align-items: center;
            height: 50px; /* Adjust the height for each option */
            width: 50px;
             /* Adjust the width for each option */
        }
        .contact-option3 {
            background-color: #fff;
            background-image: url('/img/location.png');
            background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            padding: 12px;
            text-align: center;
            display: flex;
            flex-direction: column;
            align-items: center;
            height: 50px; /* Adjust the height for each option */
            width: 50px;
             /* Adjust the width for each option */
        }
        .icon {
            font-size: 2em;
            margin-bottom: 10px;
        }

        .phone-icon {
            color: #009688; /* Material Design Teal */
        }

        .mail-icon {
            color: #2196F3; /* Material Design Blue */
        }

        .location-icon {
            color: #FF9800; /* Material Design Orange */
        }

        @media only screen and (max-width: 600px) {
      main {
        flex-direction: row;
        overflow-x: auto;
        margin-bottom: 20px;
      }
    }
</style>
</head>
<body>

{{-- <div class="my">
<div class="tel-div">
    <a href="tel:+918105055016">
        <img class="hoy" src="/img/teli1.jpeg" alt="Telephone Image" height="400px" width="400px">
    </a>
    <div class="content">
        <br> 
        <p class="blink mb-0">
            <i class="bi bi-arrow-up"></i>
            <h6 class="blink">Click On Above Phone to talk with Our Experts</h6>
        </p>
    </div>
</div>
<div class="whatsapp-div">
    <a href="whatsapp://send?phone=7996764327">
        <img src="/img/FD70A0AB-A651-4D8B-B66E-19AB54D18037_4_5005_c-removebg-preview.png" alt="" height="300px" width="300px">
    </a>
    
</div>
</div> --}}
<div class="me">
          
    <div class="phone-container">
      <h1><img src="" alt=""></h1>
      <main>
          <a href="tel:+918369835402"><div class="contact-option1">
          
         
        </div></a>
        <a href="https://maps.app.goo.gl/KArKDB29ok7YCBJC9"><div class="contact-option3">
             
        </div></a>
          
        <a href="whatsapp://send?phone=8369835402"><div class="contact-option2">
            
        </div></a>
      </main>
  </div>
 
</div>
<p class="blink mb-0">
    <i class="bi bi-arrow-up"></i>
    <h6 class="blink">Click On Above Apps to Connect with Our Experts</h6>
</p>


@include('footer')