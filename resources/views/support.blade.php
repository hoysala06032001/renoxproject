@include('header')
<div class="container-fluid page-header py-5 mb-5">
    <div class="container py-5">
        <h1 class="display-3 text-white mb-3 animated slideInDown" style="color: #C29600">Contact</h1>
        <nav aria-label="breadcrumb animated slideInDown">
            <!-- <ol class="breadcrumb">
                <li class="breadcrumb-item"><a class="text-white" href="index.html">Home</a></li>
                <li class="breadcrumb-item"><a class="text-white" href="about.html">About</a></li>
                <li class="breadcrumb-item" ><a href="service.html">service</a></li>
            </ol> -->
        </nav>
    </div>
</div>
<!-- Page Header End -->


<!-- Contact Start -->
<h1 align="center"><span style="color: #C29600">OUR WEBSITE IS OPEN 24/7</span></h1><br>
<h3 align="center" style="color:gray ;">WE ARE IN THE OFFICE AND WORKSHOP</h3><br>
<h5 align="center">MONDAY - FRIDAY 9.00-5.00PM</h5><br>
<h5 align="center">Saturday/Sunday Closed</h5><br>
<h5 align="center">Bank Holidays Closed</h5>
@include('footer')
