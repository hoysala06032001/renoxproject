@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/jewellery_apprisal.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600">Why Choose Renox for Jewelry Appraisal</h3>
        <h6>Accurate Assessments</h6>
        <p align="justify">Our team of certified gemologists and appraisers has the knowledge and expertise to accurately evaluate the worth of your jewelry, taking into account market trends and gemological factors.</p>
        <h6>Transparency</h6>
        <p align="justify">We believe in transparency and provide detailed documentation for every appraisal, ensuring you have a clear understanding of the value and characteristics of your jewelry.</p>
        <h6>Customized Appraisal</h6>
        <p align="justify">Whether it's a single piece or an entire collection, our appraisal services are tailored to your specific needs, providing you with the information you require.</p>
    </div>
</div>
@include('footer')