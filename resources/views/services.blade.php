@include('header')
<div class="container-xxl py-3">
    <div class="container">
        <!-- <div class="section-title text-center"> -->
        <h1 align="center" class="display-3  mb-3 animated slideInDown" style="color: #C29600"> Our Service</h1>
    </div>
    <div class="row g-4">
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="/img/serrr1 3.jpg" alt="" >
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">
                        BRACELET & NECKLACE
                        SHORTENING & LENGTHENING</h4>
                    <p>At Renox, our professional bracelet and necklace shortening and lengthening services.</p>
                    <a class="fw-medium" href="{{url('services1')}}" style="color: #C29600">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="img/post_earring.jpg" alt="">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">EARRINGS POST REPLACEMENT SERVICE</h4>
                    <p> Earrings are more than just accessories; they are statements of style.</p>
                    <a class="fw-medium" href="{{url('services2')}}" style="color: #C29600">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.5s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="img/earring_screw.jpg" alt="">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">EARRINGS SCREW BACK SERVICING</h4>
                    <p>Earrings are not just adornments; they are a reflection of your style and taste.</p>
                    <a class="fw-medium" href="{{url('services3')}}" style="color: #C29600">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="img/soldering.jpg" alt="">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">JEWELRY SOLDERING SERVICE</h4>
                    <p>Jewelry is more than just decorative pieces they are timeless expressions of your style.</p>
                    <a class="fw-medium" href="{{url('services4')}}" style="color: #C29600">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.3s">
            <div class="service-item">
                <div class="overflow-hidden" style="max-width: 100%; height: auto;">
                    <img class="img-fluid" src="img/clasp.jpg" alt="" style="height: auto;">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">JEWELRY CLASP REPLACEMENT</h4>
                    <p>Jewelry is a reflection of your personal style and holds significant sentimental value.</p>
                    <a class="fw-medium" href="{{url('services5')}}" style="color: #C29600">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.5s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="img/claws.jpg" alt="">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">JEWELRY CLAWS RETAPPED</h4>
                    <p>The claws that secure your precious gemstones may become worn, loose, or damaged.</p>
                    <a class="fw-medium" href="{{url('services6')}}" style="color: #C29600">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="img/jewellery_cleaning.jpg" alt="">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">JEWELRY CLEANING SERVICE</h4>
                    <p>We understand the importance of maintaining your jewelry's radiant shine.</p>
                    <a class="fw-medium" href="{{url('services7')}}" style="color: #C29600">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="img/engraving.jpg" alt="">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">JEWELRY ENGRAVING SERVICE</h4>
                    <p>Personalized jewelry, engraved with names, dates, or special messages.</p>
                    <a class="fw-medium" href="{{url('services8')}}" style="color: #C29600">Read More<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="img/jewellery_polishing.jpg" alt="">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">JEWELRY POLISHING SERVICE</h4>
                    <p>Jewelry, with its timeless elegance, captures moments and emotions that define our lives.</p>
                    <a class="fw-medium" href="{{url('services9')}}" style="color: #C29600">Click here<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="service-item">
                <div class="overflow-hidden">
                    <img class="img-fluid" src="/img/piercing.jpg" alt="">
                </div>
                <div class="p-4 text-center border border-5 border-light border-top-0">
                    <h4 class="mb-3">PIERCING SERVICE</h4>
                    <p>At RENOX Jewelry, our skilled ear piercers have undergone rigorous training by industry experts.</p>
                    <a class="fw-medium" href="{{url('services10')}}" style="color: #C29600">Click here<i class="fa fa-arrow-right ms-2"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Service End -->
@include('footer')