@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/jewellery_cleaning.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600">JEWELRY CLEANING</h3>
        <p align="justify">Our comprehensive cleaning services can breathe new life into your jewelry, restoring its sparkle and leaving it looking as radiant as if it just came from the showroom. Whether it's gold, silver, or platinum, we can effectively clean any metal, ensuring your jewelry regains its pristine appearance.</p>
        <p align="justify">For pieces that have endured regular wear, particularly those worn daily, a thorough cleaning can unveil surprising results, revealing the true brilliance of your items. You can opt for our cleaning service separately, or if your jewelry is sent in for repairs, we automatically include a professional cleaning as part of the process. We advise sending in sets of jewelry that are often worn together. For instance, if you wear an engagement ring alongside a wedding band, sending them in together ensures a consistent sparkle. After receiving your freshly cleaned jewelry, you may notice a stark contrast with other pieces, especially earrings, which tend to be overlooked and can appear dull in comparison. Make your entire collection shine by including all your regularly worn items in the cleaning process.</p>
    </div>
</div>
@include('footer')