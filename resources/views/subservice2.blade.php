@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/rosegold.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600"">Rhodium White Polishing</h3>
        <p align="justify">At Renox, we take pride in offering top-notch rhodium white polishing services to rejuvenate and enhance the brilliance of your cherished jewelry pieces. Our dedicated team of expert jewelers specializes in providing meticulous care to your white gold and platinum jewelry through our premium rhodium white polishing services.</p>
        <h4 align="center">Why Choose Renox for Rhodium White Polishing?</h4>
        <p align="justify"><h5>Expert Craftsmanship: </h5> <p align="justify">Our team consists of highly skilled jewelers with a passion for preserving the beauty of white gold and platinum jewelry.</p></p>
        <p align="justify"><h5>Quality Rhodium Plating:</h5> <p align="justify">We use high-quality rhodium plating to ensure a brilliant and durable finish for your jewelry.</p></p>
        <p align="justify"><h5>Personalized Service:</h5> <p align="justify">We understand that each piece of jewelry is unique. Our personalized service allows you to discuss your preferences and receive customized rhodium white polishing.</p></p>
        <p align="justify"><h5>Prompt Turnaround:</h5> <p align="justify">We value your time and strive to provide efficient rhodium white polishing services with a quick turnaround, allowing you to enjoy your rejuvenated jewelry sooner.</p></p>
    </div>
</div>
@include('footer')