@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/earring_screw.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600">EARRINGS SCREW BACK SERVICING</h3>
        <p align="justify">For enhanced security, consider opting for our specific screw-back earring posts, known for their exceptional safety features. The butterfly is intricately screwed around the post, offering a robust closure. These versatile screw-backs can be seamlessly attached to any earring, making them particularly recommended for high-value pieces.</p>
        <p align="justify">While they may pose a slight learning curve for those unaccustomed to handling them, especially during the initial use, the added security they provide is undoubtedly worthwhile. The post and butterfly are meticulously crafted from the same metal as the rest of the earring, ensuring a cohesive and aesthetically pleasing design.</p>
        <p align="justify">For earrings of significant value or sentimental importance, the screw-back option stands out as the most secure method of attachment. Invest in the longevity and safety of your cherished earrings by considering the reliability of our screw-back earring posts.</p>
    </div>
</div>
</div>
@include('footer')
