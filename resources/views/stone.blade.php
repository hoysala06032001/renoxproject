@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="img/custom_jewellery.avif" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600">Why Choose Renox for Stone Replacement</h3>
        <h6>Expertise</h6>
        <p align="justify">Our team of skilled jewelers has years of experience in gemology and stone replacement.
            We are committed to delivering top-quality craftsmanship that exceeds your expectations.</p>
        <h6>Gemstone Knowledge</h6>
        <p align="justify">At Renox, we're passionate about gemstones. We'll work closely with you to select the
            perfect replacement stone, ensuring it matches the original gem in cut, color, clarity, and size.</p>
        <h6>Customization</h6>
        <p align="justify">We understand the uniqueness of each piece of jewelry. Whether you need an exact match or
            want to upgrade your gemstone, our artisans can customize the replacement to your exact specifications.
        </p>
        <h6>Preserving the Original Design</h6>
        <p align="justify">ur talented jewelers take pride in replicating the original design, ensuring that your
            jewelry maintains its aesthetic integrity.</p>
        <h3 style="color: orange;">Our Stone Replacement Services</h3>
        <h6>Exact Match Replacement:</h6>
        <p align="justify">If you've lost a gemstone or it's been damaged, we can find or cut a replacement that
            mirrors the original gem, so your jewelry remains as beautiful as ever.</p>
        <h6>Upgrading Stones</h6>
        <p align="justify">Looking for a fresh look? We can help you choose a higher-grade gemstone or a different
            type of gem to elevate the overall appearance of your jewelry.</p>
        <h6>Restoration of Vintage Pieces</h6>
        <p align="justify">Vintage jewelry often has sentimental value. Our network of suppliers allows us to source
            rare and unique gemstones to restore your vintage jewelry's authenticity.
        </p>
        <h3 style="color: orange;">The Renox Process</h3>
        <h6>Consultation</h6>
        <p align="justify">
            We start by understanding your jewelry's history and your vision for the replacement stone. We'll
            provide expert guidance on stone choices and design options.</p>
        <h6>Stone Selection</h6>
        <p align="justify">With access to a vast array of gemstone suppliers, we carefully select a replacement
            stone that meets your preferences and specifications.</p>
        <h6>Artisan Craftsmanship</h6>
        <p align="justify">Our skilled jewelers expertly set the new stone, ensuring it's secure and beautifully
            integrated into your jewelry.</p>
        <h6>Quality Assurance</h6>
        <p align="justify">We inspect every detail to guarantee that your jewelry looks stunning and stands the test
            of time.</p>

@include('footer')