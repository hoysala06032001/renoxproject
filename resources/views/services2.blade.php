@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/post_earring.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600">EARRINGS POST REPLACEMENT</h3>
        <p align="justify">
            Revitalize your beloved earrings by opting for our earring post soldering service. We skillfully attach new earring posts using matching precious metals such as 18ct, silver, or platinum. This distinctive method not only ensures the secure attachment of your earrings but also provides a reliable solution to prevent the butterfly from slipping off.</p>
        <p align="justify">
            It's not uncommon for individuals to overlook the possibility of repairing broken earrings, often assuming it may not be financially viable. However, if your earrings hold sentimental or monetary value, or if you simply cherish wearing them, we highly recommend considering our earring post soldering service.
        </p>
        <p align="justify">If you've noticed your earrings becoming slightly 'wobbly' or less secure than before, it's advisable to address the issue promptly. By opting for this repair, you can prevent potential mishaps such as earrings falling off or, worse still, breaking while worn. Ensure the longevity of your cherished earrings by entrusting us with the secure and professional attachment of new earring posts.</p>
    </div>
</div>

@include('footer')