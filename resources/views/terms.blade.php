@include('header')
<div class="container my-2">
    <h1 align="center"><span style="color: #c29600">Terms</span> & <span>Conditions</span></h1>
<h3 style="color: gold" align="center">Terms & Conditions - Renox</h3>
<p align="justify">Renox.kods.app is the jewelry repair website that allows consumers to browse, select and book for jewelry repair services. Renox.kods.app provides services to you subject to the terms and conditions included in this Terms of Use and other customer service pages to help make your service experience with Renox as enjoyable and problem-free as possible. Please read them carefully. By visiting or using this website or service, you acknowledge that you have read and understood, and agree to be bound by, these Terms of Use. You also agree to comply with all applicable laws and regulations, including Copyright and Trademark laws. If you do not agree to these terms, please do not use the renox.kods.app website. If you would like to provide feedback about the renox.kods.app website or recommend a way we can improve the repair service experience, please write to renoxrsm@gmail.com, If you have any questions about these Terms and Conditions, please contact Renox on renoxrsm@gmail.com .</p>
<h3>Acceptance of Terms and Conditions</h3>
<p align="justify">Your privacy is important to us and we will protect it.
    Renox collects, stores, processes and uses your information in accordance with Renox's Privacy Policy. By using the website and/ or by providing your information, you consent to the collection and use of the information you disclose on the website by Renox in accordance with Renox's Privacy Policy. Please review Renox's Privacy Policy </p>
    <h3>Ownership and Copyright</h3>
<p align="justify">This Renox.kods.app website is owned and operated by Renox Private Limited. Unless otherwise noted, all design and content included on this web site, including text, graphics, logos, icons, images, artwork, products, audio and video clips and software is the property of Renox Private Limited (or is used under license to Renox) and is protected by Indian Copyright Act, 1957 and international copyright laws</p>
<h3>Services</h3>
<p align="justify">Renox offers repair and restoration services for jewelry items. These services are subject to separate agreements and pricing, which will be provided upon request.</p>
<h3>Privacy Policy</h3>
<p align="justify">Your use of the Website is also governed by our Privacy Policy, which can be found at https://renox.kods.app/privacy. By using the Website, you consent to the collection and use of your personal information as described in the Privacy Policy </p>
<h3>User Conduct</h3>
<p>When using the Website, you agree not to:
    <p align="justify">a. Use the Website for any unlawful or unauthorized purpose.</p>
    <p align="justify"> b. Engage in any activity that could harm, disable, overburden, or impair the Website.</p>
    <p align="justify">c. Use automated systems, such as bots or crawlers, to access or interact with the Website.</p>
    <p align="justify">d. Attempt to gain unauthorized access to any part of the Website, user accounts, or computer systems or networks associated with the Website.</p>
    <p align="justify">e. Upload, post, or transmit any content that is harmful, offensive, defamatory, or violates the rights of others</p>
    <h3>Limitation of Liability</h3>
<p align="justify">Renox shall not be liable for any direct, indirect, incidental, special, consequential, or punitive damages arising out of or in connection with your use of the Website or the services provided.</p>
<h3>Your account and registration</h3>
<p align="justify">If you access or use the Renox website, you are responsible for maintaining the confidentiality of your account and for restricting access to your computer. You agree to be responsible for all activities that occur under your account or password. You agree, to provide true, accurate, current and complete information about yourself as prompted by Website registration form or provided by you as a visitor or user of a third-party site through which you access the Website. You agree to notify Renox immediately of any unauthorized use of your account or any other breach of security. Renox reserves the right to refuse service, terminate accounts or remove or edit content in its sole discretion. You hereby grant us a non-exclusive, worldwide, perpetual, irrevocable, royalty-free, sub-licensable (through multiple tiers) right to exercise the copyright, publicity, and database rights you have in your information, in any media or medium now known or developed, produced, invented or used in future, with respect to your information.</p>
<h3>Disclaimer of Warrantiesy</h3>
<p align="justify">The Website is provided "as is" and "as available" without any warranties of any kind, either express or implied, including, but not limited to, the implied warranties of merchantability, fitness for a particular purpose, or non-infringement. You agree that your access to, and use of Renox website is at your own risk. Renox will not be liable for any damages of any kind arising from use of the website, including, without limitation, indirect, incidental, punitive or consequential damages.</p>
<h3>Indemnification</h3>
<p align="justify">You agree to indemnify and hold Renox and its services, employees, and agents harmless from any claims, liabilities, damages, costs, or expenses arising out of or in connection with your use of the Website or any violation of these Terms</p>
<h3>Dispute resolution</h3>
<p align="justify">Any dispute arising with respect to this Agreement shall be resolved by negotiation between the parties or, if necessary, by resort to an appropriate court located in Mumbai, India.</p>
<h3>Transaction status</h3>
<p align="justify">In the event the bank rejects to honour any payment transaction made by a user towards an order, Renox.kods.app shall have the right to refuse to ship the order to the user without any liability whatsoever.
    In the event the payment gateway selected by the user has flagged the transaction as fraudulent due to any reason, Renox.kods.app shall have the right to refuse to ship the order to the user without any liability whatsoever. In such a case, the user may receive a call from our Customer Service Team for relevant documents required to confirm the transaction, and if the fraudulent flag is not removed by the payment gateway, Renox.kods.app may not be able to fulfil the order. The paid amount will be refunded via the same payment mode.</p>
    <h3>Governing Law</h3>
    <p align="justify">These Terms are governed by and construed in accordance with the laws of Indian Jurisdiction, without regard to its conflict of law principles</p>
    <h3>Changes to Terms</h3>
    <p align="justify">Renox reserves the right to update or modify these Terms at any time without prior notice. It is your responsibility to review these Terms periodically.</p>
    <h3>Termination</h3>
    <p align="justify">We reserve the right to terminate or suspend your account and access to the Website at our sole discretion, without notice, for any reason, including, but not limited to, violation of these Terms.</p>
    <h3>Contact Information</h3>
    <p align="justify">If you have any questions or concerns about these Terms, please contact us at renoxrsm@gmail.com.
        By using the Renox website, you acknowledge that you have read, understood, and agree to these Terms and Conditions</p>
</div>

@include('footer')