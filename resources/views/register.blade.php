<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/img/logo-removebg-preview.png" rel="icon">
    <title>Repair</title>
    <style>
    @import url('https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css');
    body{
        background: linear-gradient(to bottom, rgb(142, 172, 231), #33ccff);
           backdrop-filter: blur(10px);
        }
</style>
</head>
<body>
<div class="container mx-auto max-w-md p-8 bg-transparent rounded shadow-md mt-20">
    <h2 class="text-center text-2xl font-bold mb-4">Jewelry Repair Form</h2>
    <form id="jewelryForm" action="{{url('register')}}" method="POST">
        @csrf
        <label class="block mb-2" for="name">Name:</label>
        <input type="text" id="name" name="name" required
            class="w-full p-2 border border-gray-300 rounded mb-4" placeholder="Enter your name">

        <label class="block mb-2" for="email">Email:</label>
        <input type="email" id="email" name="email" required
            class="w-full p-2 border border-gray-300 rounded mb-4" placeholder="Enter your email">

        <label class="block mb-2" for="phone">Phone Number:</label>
        <input type="tel" id="phone" name="phone" required
            class="w-full p-2 border border-gray-300 rounded mb-4" placeholder="Enter your Phone Number">

        <label class="block mb-2" for="jewelryDescription">Tell us about your jewelry:</label>
        <textarea id="jewelryDescription" name="jewelryDescription" rows="2" required
            class="w-full p-2 border border-gray-300 rounded mb-4" placeholder="About jewellery"></textarea>

        <label class="block mb-2" for="repairDescription">Description of Repair or Alteration:</label>
        <textarea id="repairDescription" name="repairDescription" rows="2" required
            class="w-full p-2 border border-gray-300 rounded mb-4" placeholder="Require"></textarea>

        <label class="block mb-2" for="gemsDescription">Any Other Comment:</label>
        <textarea id="gemsDescription" name="gemsDescription" rows="2"
            class="w-full p-2 border border-gray-300 rounded mb-4" placeholder="Jewelry Description"></textarea>

        <label class="block mb-2" for="uploadInput">Select Images</label>
        <input type="file" id="uploadInput" accept="image/*" multiple
            class="w-full p-2 border border-gray-300 rounded mb-4">

        <div id="imagePreview" class="mb-4"></div>
        <label class="block mb-2" for="uploadInput">Select Images</label>
        <input type="file" id="uploadInput" accept="image/*" multiple
            class="w-full p-2 border border-gray-300 rounded mb-4">

        <div id="imagePreview" class="mb-4"></div>

        {{-- <label class="block mb-2">Pincode:</label>
        <input type="text" id="pincode" name="pincode" placeholder="Enter Pincode" onchange="fetchAddress"
            class="w-full p-2 border border-gray-300 rounded mb-4"> --}}

            <label class="block mb-2" for="gemsDescription">Address</label>
            <textarea id="gemsDescription" name="gemsDescription" rows="2"
                class="w-full p-2 border border-gray-300 rounded mb-4" placeholder="address"> </textarea>

            <button type="button" onclick="submitForm()"
                class="bg-green-500 text-white py-2 px-4 rounded mr-2"><a href="{{url('communication')}}">Submit</a></button>
            <button type="button" class="bg-gray-500 text-white py-2 px-4 rounded"><a href="{{url('/')}}">Cancel</a></button>
        </div>
    </form>
</div>
<script>
 
    function submitForm() {
        alert('Form submitted!');
    }

    const imageInput = document.getElementById('imageInput');
    const imagePreview = document.getElementById('imagePreview');

    imageInput.addEventListener('change', function () {
        const file = this.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = function (e) {
                const previewImage = document.createElement('img');
                previewImage.id = 'previewImage';
                previewImage.src = e.target.result;
                imagePreview.innerHTML = '';
                imagePreview.appendChild(previewImage);
            };
            reader.readAsDataURL(file);
        }
    });

    function submitForm() {
        var nameInput = document.getElementById('name');
        var errorMessage = document.getElementById('errorMessage');
        var successMessage = document.getElementById('successMessage');

        if (nameInput.value.trim() === '') {
            errorMessage.style.display = 'block';
            successMessage.style.display = 'none';
        } else {
            errorMessage.style.display = 'none';
            successMessage.style.display = 'block';
        }
    }
</script>    
</body>
</html>