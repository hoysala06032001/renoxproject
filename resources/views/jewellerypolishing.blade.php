@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/jewellery_polishing.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600">Discover how Renox can illuminate the brilliance of your jewelry</h3>
        <h6>Artisanal Expertise</h6>
        <p align="justify">Our skilled jewelers are artisans with a deep appreciation for the craft. They meticulously polish each piece, reviving its natural shine and elegance.</p>
        <h6>Customized Care</h6>
        <p align="justify">We understand that every piece of jewelry is unique. Our polishing services are tailored to the specific metal and gemstones, ensuring optimal results without compromising their integrity.</p>
        <h6>Preservation</h6>
        <p align="justify">While polishing, we take great care to preserve the original design and craftsmanship of your jewelry, ensuring it retains its sentimental value</p>
    </div>
</div>
@include('footer')