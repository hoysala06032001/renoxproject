@include('header')
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .cont {
        width: 100%;
        padding: 10px;
        max-width: 800px; /* Adjusted for laptops and larger screens */
        margin: auto;
    }

    .ab{
        width: 100%; /* Make the image responsive */
        height: auto;
        max-width: 500px; /* Set a maximum width for the image */
        margin-bottom: 20px;
    }

    h3,
    h6,
    p {
        margin-top: 0;
        margin-bottom: 15px;
    }
</style>
<div class="cont my-4">
    <div align="center">
        <img class="ab" src="/img/serrr1 3.jpg" alt="" height="400px" width="500px">
    </div>
    <div align="center">
        <h3 style="color: #C29600" >BRACELET & NECKLACE SHORTENING & LENGTHENING</h3>
        <p align="justify">We specialize in various jewelry repair services, including the adjustment of bracelet or necklace lengths to cater to your specific preferences. If you desire a shorter length, our skilled craftsmen will carefully remove excess links and expertly solder the item back together. On the other hand, if you wish to extend the length, we will seamlessly integrate additional links crafted from the same metal as the existing chain.</p>
        <p align="justify">When sending your jewelry for adjustment, kindly provide us with the precise measurement you desire for the modification. Your satisfaction is our priority, and our experienced team is dedicated to ensuring your jewelry fits flawlessly according to your preferences.</p>
        
    </div>
</div>
@include('footer')